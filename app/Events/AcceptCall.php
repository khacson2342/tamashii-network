<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AcceptCall implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $token;
    public $id;

    public function __construct($token, $id)
    {
        $this->token = $token;
        $this->id = $id;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('accept-call.' . $this->id);
    }

    public function broadcastAs()
    {
        return 'accept-call';
    }
}
