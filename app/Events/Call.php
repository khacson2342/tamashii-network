<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Call implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $token;
    public $id;

    public function __construct($token, $id)
    {
        $this->token = $token;
        $this->id = $id;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('call.' . $this->id);
    }

    public function broadcastAs()
    {
        return 'call';
    }
}
