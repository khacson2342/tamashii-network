<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PasswordChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'curPass'  => 'required',
            'newPass'  => 'required|min:6',
            'confPass' => 'required|same:newPass',
        ];
    }

    public function messages()
    {
        return [
            'curPass.required'  => 'Current password cannot be empty',
            'newPass.required'  => 'New password cannot be empty',
            'confPass.required' => 'Password confirmation cannot be empty',
            'confPass.same'     => 'New password and confirm password does not match',
            'newPass.min'       => 'Password at least 6 characters',
        ];
    }
}
