<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class registerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'           => 'required|email|unique:users,email',
            'password'        => 'required|min:6',
            'name'            => 'required|max:30',
            'repeat-password' => 'required|same:password',
            'phone'           => 'required|unique:users,phone',
            'gender'          => 'required',
            'dob'             => 'required',
            'description'     => 'required|max:191',
            'avatar'          => 'required|image|mimes:image/jpeg,image/jpg,png|max:2048'
        ];
    }
    public function messages()
    {
        return [
            'email.required'       => __('Email is required') ,
            'email.email'          => __('Invalid email'),
            'email.unique'         => __('Email exist'),
            'name.required'        => __('Name is required'),
            'name.max'             => __('Max length is 30'),
            'password.required'    => __('Password is required'),
            'repeat-password.same' => __('Retype password not match'),
            'password.min'         => __('Password at least 6 characters'),
            'phone.required'       => __('Phone is required'),
            'phone.unique'         => __('Phone exist'),
            'gender.required'      => __('Gender is required'),
            'dob.required'         => __('Date of birth is required'),
            'description.required' => __('Description is required'),
            'description.max'      => __('Max length is 191'),
            'avatar.required'      => __('Avatar is required'),
            'avatar.mimes'         => __('File must be image'),
            'avatar.max'           => __('Max 2MB')
        ];

    }
}
