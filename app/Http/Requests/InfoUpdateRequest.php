<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class InfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $date = Carbon::now();
        return [
            'name'        => 'required|max:30',
            'dob'         => 'date|before_or_equal:'.$date,
            'phone'       => 'required|max:11',
            'description' => 'required|max:3000',
            'avatar'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
    public function messages()
    {
        return [
            'name.max'               => 'Name can not greater than 30 characters',
            'name.required'          => 'Name can not be empty',
            'dob.date'               => 'Please select a valid Date',
            'dob.before_or_equal'    => 'Date of birth can not be greater than today',
            'phone.required'         => 'Phone number can not be empty',
            'phone.max'              => 'Phone can not greater than 11 digits',
            'description.required'   => 'Description can not be empty',
            'description.max'        => 'Description can not greater than 3000 characters',
            'avatar.max'             => 'Can not upload avatar greater than 2MB',
            'avatar.mimes'           => 'Uploaded file is not a valid image',
        ];
    }
}
