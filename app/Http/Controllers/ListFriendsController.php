<?php

namespace App\Http\Controllers;

use App\Friend;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListFriendsController extends BaseController
{

    public function index()
    {

        $userId = $this->user->id;
        $allOtherUsers = User::select(['id', 'name', 'avatar', 'phone', 'gender', 'dob', 'email', 'description'])->where('id', '!=', $userId)->get();

        $listFriends    = collect();
        $listOther      = collect();
        $listSents      = collect();
        $listPendings   = collect();

        foreach ($allOtherUsers as $other) {
            $otherId = $other->id;
            $status = $this->getFriendStatusIfExist($otherId);
            if (!$status['isOther']) {
                if ($status['isFriend']) {
                    $listFriends->push($other);
                } else {
                    if ($status['isSender']) {
                        $listSents->push($other);
                    } else {
                        $listPendings->push($other);
                    }
                }
            } else {
                $listOther->push($other);
            }
        }

        return view('user.list-friends', [
            'user'          => $this->user,
            'listFriends'   => $listFriends,
            'listSents'     => $listSents,
            'listPendings'  => $listPendings,
            'listOther'     => $listOther,
        ]);
    }
}
