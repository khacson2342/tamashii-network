<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordChangeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordUpdateController extends BaseController
{

    public function changePass(PasswordChangeRequest $request)
    {

        $curPass = $request->curPass;
        $newPass = $request->newPass;
        if (Hash::check($curPass, $this->user->password)) {
            $this->user->update(['password' => Hash::make($newPass)]);
            return redirect()->back()->withSuccess('Success! Your Password has been changed!');
        } else {
            return redirect()->back()->withErrors('Your current password is incorrect');
        }
    }
}
