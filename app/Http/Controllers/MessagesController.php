<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessagesController extends BaseController
{
    public function index()
    {
        return view('user.message', [ 'user' => $this->user] );
    }

    public function detail($id)
    {
        return view('user.message-detail', [ 'user' => $this->user, 'userId' => $id ]);
    }
}
