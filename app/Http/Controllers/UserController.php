<?php

namespace App\Http\Controllers;

use App\User;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\loginRequest;
use App\Http\Requests\registerRequest;
use App\PropPublicUser;

class UserController extends Controller
{
    public function getLogin()
    {
        return view('user.login');
    }
    
    public function postLogin(loginRequest $request)
    {
        $arr = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($arr)) {
            return redirect('/my-profile')->with('status', 'Login Success');
        } else {
            return redirect()->back()->with('status', 'Email or password is incorrect');
        }
    }

    public function getRegister()
    {
        return view('user.register');
    }
    public function postRegister(registerRequest $request)
    {
        $user = new User();
        $user->email      = $request->email;
        $user->password    = Hash::make($request->password);
        $user->phone       = $request->phone;
        $user->name        = $request->name;
        $user->dob         = $request->dob;
        $user->gender      = $request->gender;
        $user->description = $request->description;
        $user->avatar     = $request->avatar;
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $avatarName = time() . '-' . $avatar->getClientOriginalName();
            $path = public_path('/avatar');
            $avatar->move($path, $avatarName);
            $user->avatar = $avatarName;
        }
        $user->save();
        $propPublicUser = new PropPublicUser();
        $propPublicUser->user_id = $user->id;
        $propPublicUser->phone = 1;
        $propPublicUser->gender = 1;
        $propPublicUser->dob = 1;
        $propPublicUser->email = 1;
        $propPublicUser->description = 1;
        $propPublicUser->save();
        
        return redirect('/login')->with('status', 'Create Account Success');
    }
}
