<?php

namespace App\Http\Controllers;

use App\Friend;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    protected $user;

    public function callAction($method, $parameters)
    {
        $userId = Auth::id();
        $this->user = User::find($userId);
        return parent::callAction($method, $parameters);
    }

    public function getFriendStatusIfExist($otherId)
    {
        $recipient = Friend::where([
            ['user_id', $otherId],
            ['friend_id', $this->user->id]
        ])->first();
        $sender  = Friend::where([
            ['user_id', $this->user->id],
            ['friend_id', $otherId]
        ])->first();
        $friend = $sender ?? $recipient;

        $dataArray = [
            'isFriend'      => false,
            'accepted_api'  => false,
            'isSender'      => false,
            'isRecipient'   => false,
            'isOther'       => false,
        ];
        $dataArray['friend'] = $friend;

        if (($recipient != null) || ($sender != null)) {
            if ($friend->accepted_request_friend == 1) {
                $dataArray['isFriend'] = true;
                if ($friend->accepted_api == 1) {
                    $dataArray['accepted_api'] = true;
                }
            } else {
                if ($friend->user_id == $this->user->id) {
                    $dataArray['isSender'] = true;
                } else {
                    $dataArray['isRecipient'] = true;
                }
            }
        } else {
            $dataArray['isOther'] = true;
        }
        return $dataArray;
    }
}
