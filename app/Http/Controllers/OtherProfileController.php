<?php

namespace App\Http\Controllers;

use App\Friend;
use App\PropPublicUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OtherProfileController extends BaseController
{

    public function index($id)
    {
        $userId = $this->user->id;
        if ($id == $this->user->id)
            return redirect()->route('my-profile');
        $otherId = $id;
        $other = User::find($otherId);
        $propPublicOther = $other->publicProp;

        $status = $this->getFriendStatusIfExist($otherId);
        return view('user.other-profile', [
            'user'            => $this->user,
            'other'           => $other,
            'propPublicOther' => $propPublicOther,
            'sentRequest'     => !$status['isOther'],
            'isFriend'        => $status['isFriend'],
            'acceptedApi'     => $status['accepted_api'],
            'isSender'        => $status['isSender'],
        ]);
    }

    public function sendRequest(Request $request)
    {
        $otherId = $request->input('otherId');
        $friend = new Friend();
        $friend->user_id = $this->user->id;
        $friend->friend_id = $otherId;
        $friend->accepted_request_friend = 0;
        $friend->accepted_api = 0;
        $friend->save();
        return redirect()->back();
    }

    public function unfriend(Request $request)
    {
        $otherId = $request->input('otherId');
        $recipient = Friend::where([
            ['user_id', $otherId],
            ['friend_id', $this->user->id]
        ])->first();
        $sender  = Friend::where([
            ['user_id', $this->user->id],
            ['friend_id', $otherId]
        ])->first();
        $recipient->delete();
        $sender->delete();
        return redirect()->back();
    }

    public function disableApi(Request $request)
    {
        $otherId = $request->input('otherId');
        $data = $this->getFriendStatusIfExist($otherId);
        $friend = $data['friend'];
        if ($friend) {
            $friend->accepted_api = 0;
            $friend->save();
        }
        return redirect()->back();
    }

    public function enableApi(Request $request)
    {
        $otherId = $request->input('otherId');
        $data = $this->getFriendStatusIfExist($otherId);
        $friend = $data['friend'];
        if ($friend) {
            $friend->accepted_api = 1;
            $friend->save();
        }
        return redirect()->back();
    }

    public function undoRequest(Request $request)
    {
        $otherId = $request->input('otherId');
        $friend = Friend::where('user_id', $this->user->id)->where('friend_id', $otherId)->first();
        if ($friend) $friend->delete();
        return redirect()->back();
    }

    public function acceptRequest(Request $request)
    {
        $otherId = $request->input('otherId');

        $sender = Friend::where('user_id', $otherId)->where('friend_id', $this->user->id)->first();
        $sender->accepted_request_friend = 1;
        $sender->save();

        $friend = new Friend();
        $friend->user_id = $this->user->id;
        $friend->friend_id = $otherId;
        $friend->accepted_request_friend = 1;
        $friend->accepted_api = 0;
        $friend->save();
        return redirect()->back();
    }
}
