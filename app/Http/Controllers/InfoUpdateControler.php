<?php

namespace App\Http\Controllers;

use App\Http\Requests\InfoUpdateRequest;
use Illuminate\Http\Request;

class InfoUpdateControler extends BaseController
{

    public function index()
    {
        return view('user.info-updating', ['user' => $this->user]);
    }

    public function update(InfoUpdateRequest $request)
    {
        $user = $this->user;
        $user->name         = $request->name;
        $user->dob          = $request->dob     ?? $user->dob;
        $user->phone        = $request->phone;
        $user->description  = $request->description ?? $user->description;
        $user->avatar       = $request->avatar ?? $user->avatar;
        if ($request->hasFile('avatar')) {
            $avatar         = $request->file('avatar');
            $avatarName     = time() . '-' . $avatar->getClientOriginalName();
            $path           = public_path('/avatar');
            $avatar->move($path, $avatarName);
            $user->avatar   = $avatarName;
        }
        $user->save();
        return redirect()->back()->with('successUpdate', 'Update successfully');
    }
}
