<?php

namespace App\Http\Controllers;
use App\PropPublicUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserProfileController extends BaseController
{

    public function index()
    {
        $publicProp = $this->user->publicProp;
        return view('user.my-profile', ['user' => $this->user, 'publicProp' => $publicProp]);
    }

    public function updatePublicProp(Request $request)
    {
        $propPublicUser = PropPublicUser::where('user_id', $this->user->id)->get()->first();
        PropPublicUser::where('user_id', '=', $this->user->id)->update([
            'email'       => $request->input('emailCb') ?? 0,
            'phone'       => $request->input('phoneCb') ?? 0,
            'gender'      => $request->input('genderCb') ?? 0,
            'dob'         => $request->input('dobCb') ?? 0,
            'email'       => $request->input('emailCb') ?? 0,
            'description' => $request->input('descCb') ?? 0,
        ]);
        Session::flash('activeTabSetting', 'active');
        return redirect()->back()->withSuccess('You have successfully updated your settings');
    }
}
