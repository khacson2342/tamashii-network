<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropPublicUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prop_public_user', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->boolean('phone');
            $table->boolean('gender');
            $table->boolean('dob');
            $table->boolean('email');
            $table->boolean('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prop_public_user');
    }
}
