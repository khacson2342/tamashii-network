<?php

use App\CustomToken;
use Illuminate\Database\Seeder;

class CustomTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CustomToken::class,10)->create();
    }
}
