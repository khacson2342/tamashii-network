<?php

use App\UserToken;
use Illuminate\Database\Seeder;

class UserTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserToken::class,10)->create();
    }
}
