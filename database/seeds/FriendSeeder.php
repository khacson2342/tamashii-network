<?php

use App\Friend;
use Illuminate\Database\Seeder;

class FriendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Friend::insert([
            [
            'user_id' => 1,
            'friend_id' => 2,
            'accepted_request_friend' => true,
            'accepted_api' => false
            ],
            [
                'user_id' => 2,
                'friend_id' => 1,
                'accepted_request_friend' => true,
                'accepted_api' => true
            ],
            [
                'user_id' => 1,
                'friend_id' => 3,
                'accepted_request_friend' => true,
                'accepted_api' => false
            ],
            [
                'user_id' => 3,
                'friend_id' => 1,
                'accepted_request_friend' => true,
                'accepted_api' => false
            ],
            [
                'user_id' => 1,
                'friend_id' => 4,
                'accepted_request_friend' => false,
                'accepted_api' => false
            ],
            [
                'user_id' => 1,
                'friend_id' => 5,
                'accepted_request_friend' => true,
                'accepted_api' => true
            ],
            [
                'user_id' => 5,
                'friend_id' => 1,
                'accepted_request_friend' => true,
                'accepted_api' => true
            ],
            [
                'user_id' => 1,
                'friend_id' => 6,
                'accepted_request_friend' => true,
                'accepted_api' => true
            ],
            [
                'user_id' => 6,
                'friend_id' => 1,
                'accepted_request_friend' => true,
                'accepted_api' => true
            ],
            [
                'user_id' => 1,
                'friend_id' => 7,
                'accepted_request_friend' => true,
                'accepted_api' => false
            ],
            [
                'user_id' => 7,
                'friend_id' => 1,
                'accepted_request_friend' => true,
                'accepted_api' => false
            ],
            [
                'user_id' => 8,
                'friend_id' => 1,
                'accepted_request_friend' => false,
                'accepted_api' => false
            ]
        ]);
    }
}
