<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\PropPublicUser;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(PropPublicUser::class, function (Faker $faker) {
    static $id = 0;
    return [
        'user_id' => ++$id,
        'phone' => true,
        'gender' => true,
        'dob' => true,
        'email' => true,
        'description' => true,
    ];
});
