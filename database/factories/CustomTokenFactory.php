<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CustomToken;
use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(CustomToken::class, function (Faker $faker) {
    static $id = 0;
    return [
        'token' => Hash::make(++$id),
        'expired_at' => $faker->dateTimeBetween('now', '+1 month')
    ];
});
