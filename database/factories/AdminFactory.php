<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Admin::class, function (Faker $faker) {
    static $id = 0;
    return [
        'email' => $faker->email,
        'password' => Hash::make('123456'),
        'token' => Hash::make(++$id)
    ];
});
