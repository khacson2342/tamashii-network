<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserToken;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

$factory->define(UserToken::class, function (Faker $faker) {
    static $id = 0;
    return [
        'user_id' => ++$id,
        'user_token' => Hash::make($id),
        'accessible' => true,
    ];
});
