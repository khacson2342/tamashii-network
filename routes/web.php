<?php

use App\Events\MyEvent;
use App\Events\TestEvent;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->group(function () {

    Route::get('/other-profile/{id}', 'OtherProfileController@index')->name('other-profile');

    Route::get('/my-profile', 'UserProfileController@index')->name('my-profile');

    Route::get('/update-public-info', 'UserProfileController@updatePublicProp')->name('updateProp');

    Route::post('/send', 'OtherProfileController@sendRequest')->name('sendRequest');

    Route::post('/unfriend', 'OtherProfileController@unfriend')->name('unfriend');

    Route::post('/undoRequest', 'OtherProfileController@undoRequest')->name('undoRequest');

    Route::post('/acceptRequest', 'OtherProfileController@acceptRequest')->name('acceptRequest');

    Route::post('/enableApi', 'OtherProfileController@enableApi')->name('enableApi');

    Route::post('/disableApi', 'OtherProfileController@disableApi')->name('disableApi');

    Route::get('/friends', 'ListFriendsController@index')->name('friends');

    Route::get('/info-updating','InfoUpdateControler@index')->name('info-updating');

    Route::post('/info-updating','InfoUpdateControler@update');

    Route::post('/change-password','PasswordUpdateController@changePass')->name('changePass');
    
    //message

    Route::get('/messages','MessagesController@index')->name('message');

    Route::get('/messages/{id}','MessagesController@detail')->name('message-detail');

    Route::post('/sendMessage',function(Request $request) {
        $data = $request->all();
        broadcast( new MyEvent($data['message'],$data['userId']));
        return response()->json(['data' => $data['message']],200);
    });

    //Video call

    
});


Route::get('/login', 'UserController@getLogin')->name('login');
Route::post('/login', 'UserController@postLogin');

Route::get('/register', 'UserController@getRegister')->name('register');
Route::post('/register', 'UserController@postRegister')->name('register');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/logout', function() {
    Auth::logout();
    return view('user.login');
})->name('logout');
