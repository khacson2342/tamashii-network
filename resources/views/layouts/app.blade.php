
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/line-awesome.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/line-awesome-font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/jquery.mCustomScrollbar.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/lib/slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/lib/slick/slick-theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('workwisehtml-10/css/responsive.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>
	// Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('3ebabcb6c54ad550219d', {
        cluster: 'ap1',
        authEndpoint: '/broadcasting/auth',
        auth: {
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}',
            }
        }
    });
</script>
@yield('css')
@yield('js')
</head>


<body>
	<div class="wrapper">
        @include('partials.header', [ 'user' => $user ])

        @section('cover-sec')

        @show

		<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
                        <div class="col-12 border-1">
                            @section('content')
                            @show
                        </div>
					</div><!-- main-section-data end-->
				</div>
			</div>
		</main>
	</div><!--theme-layout end-->



<script type="text/javascript" src="{{ asset('workwisehtml-10/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('workwisehtml-10/js/popper.js')}}"></script>
<script type="text/javascript" src="{{ asset('workwisehtml-10/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('workwisehtml-10/js/jquery.mCustomScrollbar.js')}}"></script>
<script type="text/javascript" src="{{ asset('workwisehtml-10/lib/slick/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('workwisehtml-10/js/scrollbar.js')}}"></script>
<script type="text/javascript" src="{{ asset('workwisehtml-10/js/script.js')}}"></script>
</body>
</html>
