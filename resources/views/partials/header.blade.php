<header>
    <div class="container">
        <div class="header-data">
            <div class="search-bar">
                <form>
                    <input type="text" name="search" placeholder="Search...">
                    <button type="submit"><i class="la la-search"></i></button>
                </form>
            </div><!--search-bar end-->
            <nav>
                <ul>
                    <li>
                        <a href="{{ url('#')}}" title="">
                            <span><img src="{{ asset('workwisehtml-10/images/icon1.png')}}" alt=""></span>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('friends')}}" title="">
                            <span><img src="{{ asset('workwisehtml-10/images/icon4.png')}}" alt=""></span>
                            Friends
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('my-profile')}}" title="">
                            <span><img src="{{ asset('workwisehtml-10/images/icon3.png')}}" alt=""></span>
                            My Profile
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('message') }}" title="" class="not-box-open">
                            <span><img src="{{ asset('workwisehtml-10/images/icon6.png') }}" alt=""></span>
                            Messages
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('info-updating') }}" title="">
                            <span><img src="{{ asset('workwisehtml-10/images/icon5.png')}}" alt=""></span>
                            Settings
                        </a>
                    </li>

                </ul>
            </nav><!--nav end-->
            <div class="menu-btn">
                <a href="#" title=""><i class="fa fa-bars"></i></a>
            </div><!--menu-btn end-->
            <div class="user-account"  style="width:150px">
                <div class="user-info">
                    <img src="{{ asset('avatar') . '/' . $user->avatar }}" style="width: 30px; height: 30px;" alt="">
                    <a  title="">{{ $user->name }}</a>
                    {{--

                     --}}
                </div>
                <div class="user-account-settingss">
                    <h3>Online Status</h3>
                    <ul class="on-off-status">
                        <li>
                            <div class="fgt-sec">
                                <input type="radio" name="cc" id="c5">
                                <label for="c5">
                                    <span></span>
                                </label>
                                <small>Online</small>
                            </div>
                        </li>
                        <li>
                            <div class="fgt-sec">
                                <input type="radio" name="cc" id="c6">
                                <label for="c6">
                                    <span></span>
                                </label>
                                <small>Offline</small>
                            </div>
                        </li>
                    </ul>
                    <h3>Custom Status</h3>
                    <div class="search_form">
                        <form>
                            <input type="text" name="search">
                            <button type="submit">Ok</button>
                        </form>
                    </div><!--search_form end-->
                    <h3>Setting</h3>
                    <ul class="us-links">
                        <li><a href="profile-account-setting.html" title="">Account Setting</a></li>
                        <li><a href="#" title="">Privacy</a></li>
                        <li><a href="#" title="">Faqs</a></li>
                        <li><a href="#" title="">Terms &amp; Conditions</a></li>
                    </ul>
                    <h3 class="tc"><a href="{{ route('logout') }}" title="">Logout</a></h3>
                </div>
            </div>
        </div><!--header-data end-->
    </div>
</header><!--header end-->
