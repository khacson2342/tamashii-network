@extends('layouts.app', ['user' => $user])

@section('title','Info updating')

@section('content')
    @if ($errors->any())
        <div class="p-3 mb-2 bg-danger text-white">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-8">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
                        @if(session()->has('successUpdate'))
                            <div class=" p-3 mb-2 text-white bg-success">
                                {{ session()->get('successUpdate') }}
                            </div>
                        @endif
                    <div class="acc-setting">
                        <h3 class="text-center">Update informations</h3>
                        <form action="" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Name </h4>
                                        <input class="form-control" type="text" value="{{ $user->name }}" name="name">
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Date of birth </h4>
                                        <div class="form-group">
                                            <input class="textbox-n" type="date" value="{{ $user->dob }}" name="dob">
                                        </div>
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Phone</h4>
                                        <input class="form-control" type="text" value="{{ $user->phone }}" name="phone">
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Description</h4>
                                        <textarea class="form-control" rows="5" name="description">{{ $user->description }}</textarea>
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Avatar</h4>
                                        <div class="custom-file">
                                            <input type="file" class="file-input"  name="avatar">
                                        </div>
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="save-stngs">
                                <ul>
                                    <li><button type="submit">Save Setting</button></li>
                                </ul>
                            </div><!--save-stngs end-->
                        </form>
                    </div><!--acc-setting end-->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
                    <div class="acc-setting">
                        <h3 class="text-center">Change Password</h3>
                        <form action="{{ route('changePass') }}" method="post">
                            @csrf
                            @if(session('success'))
                                <div class="p-3 mb-2 bg-success text-white">
                                    <div>{{session('success')}}</div>
                                </div>
                            @endif
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Current password</h4>
                                        <input class="form-control" type="password" name="curPass">
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>New password</h4>
                                        <input class="form-control" type="password" name="newPass">
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="notbar">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Password confirmation</h4>
                                        <input class="form-control" type="password" name="confPass">
                                    </div>
                                </div>
                            </div><!--notbar end-->
                            <div class="save-stngs d-flex justify-content-center">
                                <ul>
                                    <li><button type="submit">Change password</button></li>
                                </ul>
                            </div><!--save-stngs end-->
                        </form>
                    </div><!--acc-setting end-->
                </div>
            </div>
        </div>
    </div>
@endsection
