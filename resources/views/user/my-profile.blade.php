@extends('layouts.app',[ 'user' => $user ])

@section('title',$user->name)

@section('cover-sec')
    @include('partials.cover-sec')
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="main-left-sidebar">
                <div class="user_profile">
                    <div class="user-pro-img">
                        <img class="rounded-circle" src="{{ asset('avatar') . '/' . $user->avatar }}" alt="" style="height: 170px; width: 170px">
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                    </div><!--user_pro_status end-->
                </div><!--user_profile end-->

            </div><!--main-left-sidebar end-->
        </div>
        <div class="col-lg-9">
            <div class="main-ws-sec">
                <div class="user-tab-sec">
                    <h3>{{ $user->name }}</h3>
                    <div class="star-descp">
                        <span>{{ $user->description }}</span>
                    </div><!--star-descp end-->
                    <div class="tab-feed st2">
                        <ul>
                            <li data-tab="profile"
                                @if (!Session::has('activeTabSetting'))
                                    class="active"
                                @endif
                            >
                                <a href="#" title="">
                                    <img src="images/ic1.png" alt="">
                                    <span>My profile</span>
                                </a>
                            </li>
                            <li data-tab="setting"
                                @if (Session::has('activeTabSetting'))
                                    class="active"
                                @endif
                            >
                                <a href="#" title="">
                                    <img src="images/ic2.png" alt="">
                                    <span>Profile setting</span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- tab-feed end-->
                </div><!--user-tab-sec end-->
            </div><!--main-ws-sec end-->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {{--  --}}
            <div class="product-feed-tab
                @if (!Session::has('activeTabSetting'))
                    current
                @endif
                " id="profile" >
                <div class="posts-section ">
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="h3">Phone</div>
                            <p>{{ $user->phone }}</p>
                        </div>
                    </div><!--post-bar end-->
                </div><!--posts-section end-->

                <div class="posts-section">
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="h3">Date of birth</div>
                            <p>{{ $user->dob }}</p>
                        </div>
                    </div><!--post-bar end-->
                </div><!--posts-section end-->

                <div class="posts-section">
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="h3">Email</div>
                            <p>{{ $user->email }}</p>
                        </div>
                    </div><!--post-bar end-->
                </div><!--posts-section end-->

                <div class="posts-section">
                    <div class="post-bar">
                        <div class="post_topbar">
                            <div class="h3">Gender</div>
                            <p>
                                @if ($user->gender === FEMALE)
                                    Female
                                @elseif($user->gender === MALE)
                                    Male
                                @else
                                    Unisex
                                @endif
                            </p>
                        </div>
                    </div><!--post-bar end-->
                </div><!--posts-section end-->

            </div><!--product-feed-tab end-->
            <div class="product-feed-tab
                @if (Session::has('activeTabSetting'))
                    current
                @endif
                " id="setting">
                <div class="col-lg-12">
                    @if(session('success'))
                        <div class="p-3 mb-2 bg-success text-white">
                            <div>{{session('success')}}</div>
                        </div>
                    @endif
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
                            <div class="acc-setting">
                                <h3 class="text-center">Public Info Setting</h3>
                                <form action="{{ route('updateProp') }}" method="get">
                                    @csrf
                                    <div class="notbar">
                                        <div class="row">
                                            <div class="col-lg-11">
                                                <h4>Phone</h4>
                                                <h3>{{ $user->phone }}</h3>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="checkbox" name="phoneCb" value="1"
                                                @if ($publicProp->phone == 1)
                                                    checked
                                                @endif
                                                >
                                            </div>
                                        </div>
                                    </div><!--notbar end-->
                                    <div class="notbar">
                                        <div class="row">
                                            <div class="col-lg-11">
                                                <h4>Date of birth </h4>
                                                <h3>{{ $user->dob }}</h3>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="checkbox" name="dobCb" value="1"
                                                    @if ($publicProp->dob == 1)
                                                        checked
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    </div><!--notbar end-->
                                    <div class="notbar">
                                        <div class="row">
                                            <div class="col-lg-11">
                                                <h4>Email </h4>
                                                <h3>{{ $user->email }}</h3>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="checkbox" name="emailCb" value="1"
                                                    @if ($publicProp->email == 1)
                                                        checked
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    </div><!--notbar end-->
                                    <div class="notbar">
                                        <div class="row">
                                            <div class="col-lg-11">
                                                <h4>Gender</h4>
                                                <h3>
                                                    @if ($user->gender == FEMALE)
                                                        Female
                                                    @elseif($user->gender == MALE)
                                                        Male
                                                    @else
                                                        Unisex
                                                    @endif
                                                </h3>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="checkbox" name="genderCb" value="1"
                                                    @if ($publicProp->gender == 1)
                                                        checked
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    </div><!--notbar end-->
                                    <div class="notbar">
                                        <div class="row">
                                            <div class="col-lg-11">
                                                <h4>Profile description<h3>{{ $user->description }}</h3></h4>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="checkbox" name="descCb" value="1"
                                                    @if ($publicProp->description == 1)
                                                        checked
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    </div><!--notbar end-->
                                    <div class="save-stngs d-flex justify-content-center">
                                        <ul>
                                            <li><button type="submit">Save settings</button></li>
                                        </ul>
                                    </div><!--save-stngs end-->
                                </form>
                            </div><!--acc-setting end-->
                        </div>
                    </div>
                </div>
            </div><!--product-feed-tab end-->
            {{--  --}}
        </div>
    </div>
@endsection
