@extends('layouts.app',[ 'user' => $user ])

@section('title', 'Messages')


@section('js')
    <script>
        var channel1 = pusher.subscribe("private-user.{{ Auth::user()->id ?? ''}}");
        channel1.bind('my-event', function(data) {
            var html =  `
                <div class='main-message-box st3'>
                    <div class='message-dt st3' style="float:left !important">
                        <div class='message-inner-dt'>
                            <p>${data.message}<img src='images/smley.png' alt=''></p>
                        </div><!--message-inner-dt end-->
                        <span>5 minutes ago</span>
                    </div><!--message-dt end-->
                    <div class='messg-usr-img'>
                        <img src='http://via.placeholder.com/50x50' alt=''>
                    </div><!--messg-usr-img end-->
                </div><!--main-message-box end-->
            `;
            $("#mCSB_1_container").append(html);
            console.log('has new message');
            $("#mCSB_1").animate({ scrollTop: $("#mCSB_1").prop("scrollHeight") }, "slow");
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#submitMessage').submit(function(e) {
                e.preventDefault(this);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.post('{{url('sendMessage')}}', { userId : {{ $userId }}, message : $('#text').val()},
                    function(returnedData){
                        console.log(returnedData);
                }).fail(function(){
                    console.log("error");
                });
                var t = $('#text').val();
                var html = `
                    <div class="main-message-box ta-right">
                        <div class="message-dt" style="float:right !important">
                            <div class="message-inner-dt">
                                <p>${t}</p>
                            </div><!--message-inner-dt end-->
                            <span>Sat, Aug 23, 1:08 PM</span>
                        </div><!--message-dt end-->
                        <div class="messg-usr-img">
                            <img src="http://via.placeholder.com/50x50" alt="">
                        </div><!--messg-usr-img end-->
                    </div><!--main-message-box end-->
                `;
                $("#mCSB_1_container").append(html);
                $('#text').val('');
                console.log($('#mCSB_1')[0].scrollHeight);
                $("#mCSB_1").animate({ scrollTop: $("#mCSB_1").prop("scrollHeight") }, "slow");
            });
        });
    </script>
@endsection

@section('content')
    <section class="messages-page m-0 p-0">
        <div class="container">
            <div class="messages-sec">
                <div class="row">
                    <div class="col-lg-12 col-md-12 pd-right-none pd-left-none" >
                        <div class="main-conversation-box">
                            <div class="message-bar-head">
                                <div class="usr-msg-details">
                                    <div class="usr-ms-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div>
                                    <div class="usr-mg-info">
                                        <h3>John Doe</h3>
                                        <p>Online</p>
                                    </div><!--usr-mg-info end-->
                                </div>
                                <a href="#" title=""><i class="fa fa-ellipsis-v"></i></a>
                            </div><!--message-bar-head end-->
                            <div class="messages-line" id="messages-line">
                                <div class="main-message-box">
                                    <div class="messg-usr-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div><!--messg-usr-img end-->
                                    <div class="message-dt">
                                        <div class="message-inner-dt img-bx">
                                            <img src="http://via.placeholder.com/151x125" alt="">
                                            <img src="http://via.placeholder.com/151x125" alt="">
                                            <img src="http://via.placeholder.com/151x125" alt="">
                                        </div><!--message-inner-dt end-->
                                        <span>Sat, Aug 23, 1:08 PM</span>
                                    </div><!--message-dt end-->
                                </div><!--main-message-box end-->
                                <div class="main-message-box ta-right">
                                    <div class="message-dt">
                                        <div class="message-inner-dt">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
                                        </div><!--message-inner-dt end-->
                                        <span>Sat, Aug 23, 1:08 PM</span>
                                    </div><!--message-dt end-->
                                    <div class="messg-usr-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div><!--messg-usr-img end-->
                                </div><!--main-message-box end-->
                                <div class="main-message-box st3">
                                    <div class="message-dt st3">
                                        <div class="message-inner-dt">
                                            <p>Cras ultricies ligula.<img src="images/smley.png" alt=""></p>
                                        </div><!--message-inner-dt end-->
                                        <span>5 minutes ago</span>
                                    </div><!--message-dt end-->
                                    <div class="messg-usr-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div><!--messg-usr-img end-->
                                </div><!--main-message-box end-->
                                <div class="main-message-box ta-right">
                                    <div class="message-dt">
                                        <div class="message-inner-dt">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
                                        </div><!--message-inner-dt end-->
                                        <span>Sat, Aug 23, 1:08 PM</span>
                                    </div><!--message-dt end-->
                                    <div class="messg-usr-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div><!--messg-usr-img end-->
                                </div><!--main-message-box end-->
                                <div class="main-message-box st3">
                                    <div class="message-dt st3">
                                        <div class="message-inner-dt">
                                            <p>Lorem ipsum dolor sit amet</p>
                                        </div><!--message-inner-dt end-->
                                        <span>2 minutes ago</span>
                                    </div><!--message-dt end-->
                                    <div class="messg-usr-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div><!--messg-usr-img end-->
                                </div><!--main-message-box end-->
                                <div class="main-message-box ta-right">
                                    <div class="message-dt">
                                        <div class="message-inner-dt">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor.</p>
                                        </div><!--message-inner-dt end-->
                                        <span>Sat, Aug 23, 1:08 PM</span>
                                    </div><!--message-dt end-->
                                    <div class="messg-usr-img">
                                        <img src="http://via.placeholder.com/50x50" alt="">
                                    </div><!--messg-usr-img end-->
                                </div><!--main-message-box end-->
                                <span id="putMessage">

                                </span>
                            </div><!--messages-line end-->
                            <div class="message-send-area">
                                <form id="submitMessage" method="POST">
                                    <div class="mf-field">
                                        <input type="text" id="text"  name="message" placeholder="Type a message here" autocomplete="off">
                                        <button type="submit" id="sendMessage">Send</button>
                                    </div>
                                    <ul>
                                        <li><a href="#" title=""><i class="fa fa-smile-o"></i></a></li>
                                        <li><a href="#" title=""><i class="fa fa-camera"></i></a></li>
                                        <li><a href="#" title=""><i class="fa fa-paperclip"></i></a></li>
                                    </ul>
                                </form>
                            </div><!--message-send-area end-->
                        </div><!--main-conversation-box end-->
                    </div>
                </div>
            </div><!--messages-sec end-->
        </div>
    </section><!--messages-page end-->
    <script>
        $("#mCSB_1").animate({ scrollTop: 200000 }, "slow");
    </script>
@endsection