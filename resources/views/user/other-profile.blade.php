@extends('layouts.app', [ 'user' => $user ])

@section('title',$other->name . ' Profile')

@section('cover-sec')
    @include('partials.cover-sec')
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="main-left-sidebar">
                <div class="user_profile">
                    <div class="user-pro-img">
                        <img class="rounded-circle" src="{{ asset('avatar') . '/' . $other->avatar }}" alt="" style="height: 170px; width: 170px">
                    </div><!--user-pro-img end-->
                    <div class="user_pro_status">
                        <ul class="flw-hr">
                            @if (!$sentRequest)
                                <form action="{{ route('sendRequest') }}" method="post">
                                    @csrf
                                    <input type="text" name="otherId" value={{ $other->id }} hidden>
                                    <button type="submit" class="btn btn-primary w-75"><i class="la la-plus"></i> Send Friend Request</button>
                                </form>
                            @else
                                @if ($isFriend)
                                    <form action="{{ route('unfriend') }}" method="post">
                                        @csrf
                                        <input type="text" name="otherId" value={{ $other->id }} hidden>
                                        <button type="submit" class="btn btn-danger w-75 m-1"><i class="la la-minus"></i>Unfriend</button>
                                    </form>
                                    @if ($acceptedApi)
                                        <form action="{{ route('disableApi') }}" method="post">
                                            @csrf
                                            <input type="text" name="otherId" value={{ $other->id }} hidden>
                                            <button type="submit" class="btn btn-danger w-75"><i class="la la-minus"></i>Disable Access My Api</button>
                                        </form>
                                    @else
                                        <form action="{{ route('enableApi') }}" method="post">
                                            @csrf
                                            <input type="text" name="otherId" value={{ $other->id }} hidden>
                                            <button type="submit" class="btn btn-primary w-75"><i class="la la-plus"></i>Enable Access My Api</button>
                                        </form>
                                    @endif
                                @else
                                    @if ($isSender)
                                        <form action="{{ route('undoRequest') }}" method="post">
                                            @csrf
                                            <input type="text" name="otherId" value={{ $other->id }} hidden>
                                            <button type="submit" class="btn btn-danger w-75"><i class="la la-minus"></i>Undo Friend Request</button>
                                        </form>
                                    @else
                                        <form action="{{ route('acceptRequest') }}" method="post">
                                            @csrf
                                            <input type="text" name="otherId" value={{ $other->id }} hidden>
                                            <button type="submit" class="btn btn-primary w-75"><i class="la la-plus"></i>Accept Friend Request</button>
                                        </form>
                                    @endif
                                @endif
                            @endif


                        </ul>
                    </div><!--user_pro_status end-->
                </div><!--user_profile end-->

            </div><!--main-left-sidebar end-->
        </div>
        <div class="col-lg-9">
            <div class="main-ws-sec">
                <div class="user-tab-sec">
                    <h3>{{ $other->name }}</h3>
                    <div class="star-descp">
                        <span>
                        @if ( $propPublicOther->description === 1)
                            {{ $other->description }}
                        @endif
                        </span>
                    </div><!--star-descp end-->
                </div><!--user-tab-sec end-->
            </div><!--main-ws-sec end-->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            @if (($propPublicOther->phone === 1) || $isFriend)
            <div class="posts-section ">
                <div class="post-bar">
                    <div class="post_topbar">
                        <div class="h3">Phone</div>
                        <p>{{ $other->phone }}</p>
                    </div>
                </div><!--post-bar end-->
            </div><!--posts-section end-->
            @endif

            @if (($propPublicOther->dob === 1) || $isFriend)
            <div class="posts-section">
                <div class="post-bar">
                    <div class="post_topbar">
                        <div class="h3">Date of birth</div>
                        <p>{{ $other->dob }}</p>
                    </div>
                </div><!--post-bar end-->
            </div><!--posts-section end-->
            @endif

            @if (($propPublicOther->email === 1) || $isFriend)
            <div class="posts-section">
                <div class="post-bar">
                    <div class="post_topbar">
                        <div class="h3">Email</div>
                        <p>{{ $other->email }}</p>
                    </div>
                </div><!--post-bar end-->
            </div><!--posts-section end-->
            @endif

            @if (($propPublicOther->gender === 1) || $isFriend)
            <div class="posts-section">
                <div class="post-bar">
                    <div class="post_topbar">
                        <div class="h3">Gender</div>
                        <p>
                            @if ($other->gender == FEMALE)
                                Female
                            @elseif($other->gender == MALE)
                                Male
                            @else
                                Unisex
                            @endif
                        </p>
                    </div>
                </div><!--post-bar end-->
            </div><!--posts-section end-->
            @endif

            {{--  --}}
        </div>
    </div>
@endsection
