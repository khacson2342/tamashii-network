@extends('layouts.app', ['user' => $user])

@section('title','List Friends')

@section('content')
    <div class="container">
        @if (!$listFriends->isEmpty())
            <div class="company-title mt-5">
                <h3 class="text-white text-center bg-success">All Friends</h3>
            </div><!--company-title end-->
        @endif
        <div class="companies-list">
            <div class="row">
                @foreach ($listFriends as $item)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="company_profile_info">
                            <div class="company-up-info">
                                <img src="{{ asset('avatar') . '/' . $item->avatar }}" alt="">
                                <h3>{{ $item->name }}</h3>
                            </div>
                            <a href="{{ url("other-profile/$item->id")}}" title="" class="view-more-pro">View Profile</a>
                        </div><!--company_profile_info end-->
                    </div>
                @endforeach
            </div>
        </div><!--companies-list end-->

        @if (!$listPendings->isEmpty())
            <div class="company-title mt-5">
                <h3 class="text-white text-center bg-success">Pending accept</h3>
            </div><!--company-title end-->
        @endif
        <div class="companies-list">
            <div class="row">
                @foreach ($listPendings as $item)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="company_profile_info">
                            <div class="company-up-info">
                                <img src="{{ asset('avatar') . '/' . $item->avatar }}" alt="">
                                <h3>{{ $item->name }}</h3>
                            </div>
                            <a href="{{ url("other-profile/$item->id")}}" title="" class="view-more-pro">View Profile</a>
                        </div><!--company_profile_info end-->
                    </div>
                @endforeach
            </div>
        </div><!--companies-list end-->

        @if (!$listSents->isEmpty())
            <div class="company-title mt-5">
                <h3 class="text-white text-center bg-success">Sent Request</h3>
            </div><!--company-title end-->
        @endif
        <div class="companies-list">
            <div class="row">
                @foreach ($listSents as $item)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="company_profile_info">
                            <div class="company-up-info">
                                <img src="{{ asset('avatar') . '/' . $item->avatar }}" alt="">
                                <h3>{{ $item->name }}</h3>
                            </div>
                            <a href="{{ url("other-profile/$item->id")}}" title="" class="view-more-pro">View Profile</a>
                        </div><!--company_profile_info end-->
                    </div>
                @endforeach
            </div>
        </div><!--companies-list end-->

        @if (!$listOther->isEmpty())
            <div class="company-title mt-5">
                <h3 class="text-white text-center bg-success">Other users</h3>
            </div><!--company-title end-->
        @endif
        <div class="companies-list">
            <div class="row">
                @foreach ($listOther as $item)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="company_profile_info">
                            <div class="company-up-info">
                                <img src="{{ asset('avatar') . '/' . $item->avatar }}" alt="">
                                <h3>{{ $item->name }}</h3>
                            </div>
                            <a href="{{ url("other-profile/$item->id")}}" title="" class="view-more-pro">View Profile</a>
                        </div><!--company_profile_info end-->
                    </div>
                @endforeach
            </div>
        </div><!--companies-list end-->
    </div>
@endsection
