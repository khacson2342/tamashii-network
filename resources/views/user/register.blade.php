
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Register</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<style>
		body {
			background-image: linear-gradient(to right, #ff5722, #ff9800);
			margin-top: 150px !important;
		}
		.register-left{
			text-align: center;
			color: #fff;
			padding: 30px;
		}
		.register-left img {
			margin-top: 60px;
			margin-bottom: 18px;
			width: 80px;
			animation: mover 1s infinite alternate;
		}
		.register-left p{
			padding: 20px 20px 0;
		}
		.register-left .btn-primary{
			border-radius: 1.5rem;
			border: none;
			width: 120px;
			background: #f8f8f8;
			font-weight: 600;
			color: #555;
			margin-top: 20px;
			padding: 20px;
		}
		.register-left .btn-primary:hover {
			background: #000;
		}
		.register-right {
			border: none;
			background: #f8f8f8;
			/* border-top-left-radius: 10% 50%; */
			/* border-bottom-left-radius: 10% 50%; */
			padding: 10px;
		}
		.register-right h2{
			text-align: center;
			margin-bottom: 10px;
			color: #555;
		}
		.register-form {
			/* padding: 30px; */
		}

		.register-right .btn-primary{
			float: right;
			border-radius: 1.5rem;
			border: none;
			width: 120px;
			background: #f8f8f8;
			font-weight: 600;
			color: #555;
			margin-top: 20px;
			padding: 10px;
		}
		.register-right .btn-primary:hover {
			background: #ff5722;
		}
		@keyframes mover {
			0%{transform: translate(0)};
			100%{transform: translate(-20px)};
		}
	</style>
	
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3 register-left">
				<h3>Tamashii</h3>
				<p>Already have an account ?	</p>
				<button type="button" class="btn btn-primary" > <a href="{{ route('login') }}">Login</a></button>
			</div>
			<div class="col-md-9 register-right">
				<h2>Register Here</h2>

				<form action="{{ route('register') }}" id="register" method="POST" role="form" enctype="multipart/form-data">
				@csrf
				<table>
						<div class="register-form">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email" name="email" readonly onfocus="this.removeAttribute('readonly')">
							</div>
							@error('email')
								<p class="alert alert-danger"> {{ $message }}</p>
							@enderror
						</div>
							<div class="register-form">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Name" name="name">
								</div>
								@error('name')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Password" name="password" readonly onfocus="this.removeAttribute('readonly')">
								</div>
								@error('password')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Repeat password" name="repeat-password" readonly onfocus="this.removeAttribute('readonly')">
								</div>
								@error('repeat-password')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Phone number" name="phone">
								</div>
								@error('phone')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									Male<input type="radio" name = "gender" value="0" placeholder="">
									Female<input type="radio" name = "gender" value="1" placeholder="">
								</div>
								@error('gender')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									<label for="">Date of birth</label>
									<input placeholder="Date of birth" class="textbox-n" type="text" onfocus="(this.type='date')" name="dob">
								</div>
								@error('dob')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Desscription" name="description">
								</div>
								@error('description')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
							<div class="register-form">
								<div class="form-group">
									<div class="form-group">
										<label for="exampleFormControlFile1">Avatar</label>
										<input type="file" class="form-control-file" name="avatar" id="validatedCustomFile">
									  </div>
								</div>
								@error('avatar')
									<p class="alert alert-danger"> {{ $message }}</p>
								@enderror
							</div>
				<button type="submit" value="submit" class="btn btn-primary">Register</button>

			</form>
			</div>
		</div>
	</div>
</body>
</html>
